package ec.com.blackbox.codificacionweb;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.system.ErrnoException;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class Codificacion extends AppCompatActivity implements ViewCell.OnToggledListener {
    GridLayout contenedorCodificacion;
    TextView codigoGenerado;
    ViewCell[] viewCells;
    Context context;
    String[] lados;
    String[] lados_letras;
    int contenedorLadoSelect = 0;
    int contenedorLadoSelectTemporal = 0;
    int numeroColumnas = 0;
    int numeroFilas = 0;

    ArrayList<ViewCell> escogidos = new ArrayList<ViewCell>();

    ViewCell cellAnterior = null;
    ViewCell cellAnteriorTrue = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codificacion);
        setTitle("Codificación Daño");
        this.context = this;
        this.lados = getResources().getStringArray(R.array.lados_contenedor);
        this.lados_letras = getResources().getStringArray(R.array.lados_contenedor_letra);


        final Button cambiarLado = (Button) findViewById(R.id.contenedorLado);
        this.contenedorCodificacion = (GridLayout) findViewById(R.id.contenedorCodificacion);
        this.codigoGenerado = (TextView) findViewById(R.id.codigoGenerado);

        cambiarLado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               new AlertDialog.Builder(context)
                       .setTitle("LADO CONTENEDOR")
                        .setSingleChoiceItems(lados, contenedorLadoSelect, new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialogInterface, int selectedIndex) {
                                contenedorLadoSelectTemporal = selectedIndex;
                            }
                        })
                        .setPositiveButton("Seleccionar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                contenedorLadoSelect = contenedorLadoSelectTemporal;
                                cambiarLado.setText(lados[contenedorLadoSelect]);
                                escogidos = new ArrayList<ViewCell>();
                                int rowTemp = 0;
                                int colTemp = 0;
                                if(contenedorLadoSelect==0 || contenedorLadoSelect==2 || contenedorLadoSelect==3 || contenedorLadoSelect==5 ){
                                    rowTemp = 4;
                                }else if(contenedorLadoSelect==6 || contenedorLadoSelect==7 || contenedorLadoSelect==8){
                                    rowTemp = 2;
                                }
                                if(contenedorLadoSelect==0 || contenedorLadoSelect==2){
                                    colTemp = 4;
                                }else{
                                    colTemp = 10;
                                }
                                codigoGenerado.setText(lados_letras[contenedorLadoSelect]);
                                graficarCuadricula(colTemp,rowTemp);
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                contenedorLadoSelectTemporal = contenedorLadoSelect;
                            }
                        })
                        .show();
            }
        });





        // cruadricular una matriz recibiendo Rows y Cols
        //this.graficarCuadricula(int Row, int Col);

    }

    public void graficarCuadricula(final int colTemp, final int rowTemp){
        contenedorCodificacion.removeAllViews();
        final int MARGIN = 5;
        final int numOfCol = colTemp;
        final int numOfRow = rowTemp;


        this.numeroFilas = numOfRow;
        this.numeroColumnas = numOfCol;


        contenedorCodificacion.setColumnCount(numOfCol);
        contenedorCodificacion.setRowCount(numOfRow);
        viewCells = new ViewCell[numOfCol*numOfRow];
        for(int yPos=0; yPos<numOfRow; yPos++){
            for(int xPos=0; xPos<numOfCol; xPos++){
                ViewCell tView = new ViewCell(this, xPos, yPos);
                tView.setOnToggledListener(this);
                viewCells[yPos*numOfCol + xPos] = tView;
                contenedorCodificacion.addView(tView);
            }
        }

        contenedorCodificacion.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {


                int pWidth = contenedorCodificacion.getWidth();

                int w = pWidth/numOfCol;

                for(int yPos=0; yPos<numOfRow; yPos++){
                    for(int xPos=0; xPos<numOfCol; xPos++){
                        if((yPos*numOfCol + xPos)<viewCells.length){
                            GridLayout.LayoutParams params =
                                    (GridLayout.LayoutParams)viewCells[yPos*numOfCol + xPos].getLayoutParams();


                            if(numOfRow!=numOfCol && numOfRow!=4){
                                params.width = w - 2*MARGIN;
                                params.height = 100 - 2*MARGIN;
                            }else if(numOfRow==4 && numOfRow!=numOfCol){
                                params.width = w - 2*MARGIN;
                                if(yPos==0 || yPos==3){
                                    params.height = 30 - 2*MARGIN;
                                }else{
                                    params.height = 100 - 2*MARGIN;
                                }

                            }else if(numOfRow==numOfCol){
                                //cuadrado
                                if(xPos==0 || xPos==numOfCol-1){
                                    params.width = 30 - 2*MARGIN;
                                }else{
                                    params.width = 100 - 2*MARGIN;
                                }

                                if(yPos==0 || yPos==numOfRow-1){
                                    params.height = 30 - 2*MARGIN;
                                }else{
                                    params.height = 100 - 2*MARGIN;
                                }
                            }
                            params.setMargins(MARGIN, MARGIN, MARGIN, MARGIN);
                            viewCells[yPos*numOfCol + xPos].setLayoutParams(params);
                        }

                    }
                }
            }
        });
    }


    @Override
    public void OnToggled(ViewCell v, boolean touchOn) {

        this.decode(v,touchOn);
    }

    public void decode(ViewCell v, boolean touchOn){


        this.contenedorCodificacion.getChildCount();
        Boolean[][] checkedArray = new Boolean[this.numeroFilas][this.numeroColumnas];
        //Boolean yaTieneCheck = false;
        int numTouchTrue = 0;

        for(int i=0; i<this.contenedorCodificacion.getChildCount(); i++){


            ViewCell temp = (ViewCell) this.contenedorCodificacion.getChildAt(i);


            int columna = i % this.numeroColumnas;
            int fila = i / this.numeroColumnas;
            checkedArray[fila][columna] = temp.touchOn;
            if(temp.touchOn){
                numTouchTrue++;
            }


        }

        //solo para visualización
        for (int i=0; i<this.numeroFilas;i++){
            String cadena = "";
            int checkedInt = -1;
            for (int b=0; b<this.numeroFilas;b++) {
                if(checkedArray[i][b]){
                    if(checkedInt==-1 || (checkedInt+1==b || checkedInt-1==b))
                        checkedInt = b;
                }
                cadena += "\t" + checkedArray[i][b].toString();

            }
        }


        //DOOR
        // 1 2 3 4
        // H
        // T
        // B
        // G

        if(touchOn){
            this.escogidos.add(v);
        }else{
            this.escogidos.remove(v);
        }

        if(numTouchTrue>1){
            Boolean boolArriba = false;
            Boolean boolDerecha = false;
            Boolean boolAbajo = false;
            Boolean boolIzquierda = false;
            ViewCell vTemp = this.escogidos.get(this.escogidos.size()-1);


            try{ boolArriba = checkedArray[vTemp.getIdY()-1][vTemp.getIdX()]; }catch (Exception e){ boolArriba=false; }
            try{ boolDerecha = checkedArray[vTemp.getIdY()][vTemp.getIdX()+1]; }catch (Exception e){ boolDerecha=false; }
            try{ boolAbajo = checkedArray[vTemp.getIdY()+1][vTemp.getIdX()]; }catch (Exception e){ boolAbajo=false; }
            try{ boolIzquierda = checkedArray[vTemp.getIdY()][vTemp.getIdX()-1]; }catch (Exception e){ boolIzquierda=false; }

            if(boolArriba || boolDerecha || boolAbajo || boolIzquierda){

                this.codigoGenerado.setText("VÁLIDO");
            }else{

                v.touchOn=!v.touchOn;
                v.invalidate();
                new AlertDialog.Builder(context)
                        .setTitle("Error")
                        .setMessage("El panel no es correcto")
                        .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
            }

        }else{
            this.codigoGenerado.setText("VÁLIDO");
        }




//
//
//        String[] letra2 = new String[this.numeroFilas];
//        if(this.numeroFilas==2){
//            if(this.lados_letras[contenedorLadoSelect]=="U"){
//                letra2[0] = "R";
//                letra2[1] = "L";
//            }else{
//                letra2[0] = "L";
//                letra2[1] = "R";
//            }
//        }else if(this.numeroFilas==4){
//            letra2[0] = "H";
//            letra2[1] = "T";
//            letra2[2] = "B";
//            letra2[3] = "G";
//        }
//
//
//        //4 filas opciones H T B G
//        // 2 filas T-B: L R   U: R L
//
//        //formando el código
//        String codigo = this.lados_letras[contenedorLadoSelect] + letra2[v.getIdY()];
//        this.codigoGenerado.setText(codigo);

    }


}
